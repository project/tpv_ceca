<?php

namespace Drupal\tpv_ceca\PluginForm\Ceca;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tpv_ceca\TpvCecaMapper;

/**
 * Offsite form for CECA PaymentGateway.
 */
class CecaPaymentOffsiteForm extends BasePaymentOffsiteForm {

  /**
   * Override method.
   *
   * @inheritDoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $payment->getOrder();

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    // Payment gateway configuration data.
    $data['MerchantID'] = $configuration['merchant_id'];
    $data['AcquirerBIN'] = $configuration['acquirer_bin'];
    $data['TerminalID'] = $configuration['terminal_id'];

    $data['Num_operacion'] = (string) time() . '-' . $order->id();
    $data['Importe'] = strval($payment->getAmount()->getNumber() * 100);
    $data['TipoMoneda'] = TpvCecaMapper::DEFAULT_CURRENCY;
    $data['Exponente'] = TpvCecaMapper::CECA_EXP;

    $data['URL_OK'] = $form['#return_url'];
    $data['URL_NOK'] = $form['#cancel_url'];
    // Fixed in documentation.
    $data['Descripcion'] = $this->generateOperationDescription($order);
    $data['Firma'] = $this->calculateSignedOperation($configuration, $data['Num_operacion'], $data['Importe'], $data['TipoMoneda'], $data['Exponente'], $data['URL_OK'], $data['URL_NOK']);
    $data['Cifrado'] = TpvCecaMapper::DEFAULT_CIPHER;
    $data['Idioma'] = $configuration['language'];

    // Fixed in documentation.
    $data['Pago_soportado'] = "SSL";
    $data['Sesion'] = TRUE;

    $endpoint = 'https://' . $configuration['endpoint_domain'] . $configuration['endpoint_route'];
    return $this->buildRedirectForm(
      $form,
      $form_state,
      $endpoint,
      $data,
      $this::REDIRECT_POST
    );
  }

  /**
   * Calculate the signed string for the current operation.
   *
   * @param array $configuration
   *   The whole configuration of the PaymentGateway.
   * @param string $operation_number
   *   The operaton number.
   * @param string $amount
   *   The amount of the current operation.
   * @param string $currency
   *   The currency.
   * @param string $exp
   *   Fixed value from documentation.
   * @param string $url_ok
   *   URL when success.
   * @param string $url_nok
   *   URL for fail.
   *
   * @return false|string
   *   False if a problem occurs. Otherwise return the signed chain.
   */
  private function calculateSignedOperation(array $configuration, string $operation_number, string $amount, string $currency, string $exp, string $url_ok, string $url_nok) {
    $sign = '';
    $sign .= $configuration['encrypt_key'];
    $sign .= $configuration['merchant_id'];
    $sign .= $configuration['acquirer_bin'];
    $sign .= $configuration['terminal_id'];
    $sign .= $operation_number;
    $sign .= $amount;
    $sign .= $currency;
    $sign .= $exp;
    $sign .= TpvCecaMapper::DEFAULT_CIPHER;
    $sign .= $url_ok;
    $sign .= $url_nok;

    return hash('sha256', $sign);
  }

  /**
   * Generates a unique description for the operation.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   The order object.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The description.
   */
  private function generateOperationDescription(Order $order) {
    return $this->t('Operation for %id', ['%id' => $order->id()]);
  }

}
