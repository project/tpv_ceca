<?php

namespace Drupal\tpv_ceca;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Util class to map values for CECA.
 */
class TpvCecaMapper {

  /* Trait for translations. */
  use StringTranslationTrait;

  const DEFAULT_CIPHER = "SHA2";

  // Euro in ISO-4217.
  const DEFAULT_CURRENCY = "978";

  // Always 2 according to documentation.
  const CECA_EXP = "2";

  /**
   * Get the availables languages on CECA.
   *
   * @return array
   *   The array of languages.
   */
  public function getLanguages() {
    $languages = [
      '1' => $this->t('Spanish'),
      '2' => $this->t('Catalan'),
      '3' => $this->t('Basque'),
      '4' => $this->t('Galician'),
      '5' => $this->t('Valencian'),
      '6' => $this->t('English'),
      '7' => $this->t('French'),
      '8' => $this->t('German'),
      '9' => $this->t('Portuguese'),
      '10' => $this->t('Italian'),
      '11' => $this->t('Russian'),
      '12' => $this->t('Norwegian'),
    ];
    return $languages;
  }

  /**
   * Get the cipher options for the settings.
   *
   * @return string[]
   *   The array of cipher options.
   */
  public function getCipherOptions() {
    $ciphers = [
      '0' => self::DEFAULT_CIPHER,
      '1' => 'SHA21',
    ];

    return $ciphers;
  }

}
