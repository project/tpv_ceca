<?php

namespace Drupal\tpv_ceca\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class TPVPane to show information on workflows definition.
 *
 * @CommerceCheckoutPane(
 *   id = "tpv_info",
 *   label = @Translation("TPV info"),
 *   display_label = @Translation("TPV info"),
 *   wrapper_element = "fieldset",
 * )
 */
class TPVPane extends CheckoutPaneBase implements CheckoutPaneInterface {

  /**
   * Overrides method.
   *
   * @inheritDoc
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form['message'] = [
      '#markup' => '<div class="advice">' . '<div class="highlight">' . $this->t('Important note') . '</div>' . '<diiv class="note"><span>' . $this->t("You're going to be moved out to the bank page. Once you finish the payment, you'll be moved back to this website") . '</span></diiv>' . '</div>',
    ];

    return $pane_form;
  }

}
