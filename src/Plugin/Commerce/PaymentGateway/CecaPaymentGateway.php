<?php

namespace Drupal\tpv_ceca\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tpv_ceca\TpvCecaMapper;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Manual payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "ceca",
 *   label = "Ceca Gateway",
 *   display_label = "Ceca gateway",
 *   modes = {
 *     "test" = @Translation("Sandbox"),
 *     "live" = @Translation("Live"),
 *   },
 *   forms = {
 *     "offsite-payment" =
 *   "Drupal\tpv_ceca\PluginForm\Ceca\CecaPaymentOffsiteForm",
 *   },
 *   payment_type = "payment_default",
 * )
 */
class CecaPaymentGateway extends OffsitePaymentGatewayBase {

  /**
   * Overrides method.
   *
   * @inheritDoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $mapper = new TpvCecaMapper();

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID on CECA'),
      '#size' => 9,
      '#default_value' => $this->configuration['merchant_id'],
    ];
    $form['acquirer_bin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Acquirer ID on CECA'),
      '#size' => 9,
      '#default_value' => $this->configuration['acquirer_bin'],
    ];
    $form['terminal_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Terminal ID on CECA'),
      '#size' => 9,
      '#default_value' => $this->configuration['terminal_id'],
    ];
    $form['encrypt_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Encrypt key to sign operations'),
      '#size' => 9,
      '#default_value' => $this->configuration['encrypt_key'],
    ];

    $cipher_options = $mapper->getCipherOptions();

    $form['cipher'] = [
      '#type' => 'select',
      '#title' => $this->t('Cipher mechanism'),
      '#options' => $cipher_options,
      '#default_value' => $this->configuration['cipher'],
    ];

    $languages = $mapper->getLanguages();

    $form['language'] = [
      '#type' => 'select',
      '#title' => $this->t('Language on CECA'),
      '#options' => $languages,
      '#default_value' => $this->configuration['language'],
    ];

    $form['endpoint_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain for the endpoint on CECA'),
      '#size' => 9,
      '#default_value' => $this->configuration['endpoint_domain'],
    ];
    $form['endpoint_route'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Route for the endpoint on CECA'),
      '#size' => 9,
      '#default_value' => $this->configuration['endpoint_route'],
    ];

    return $form;
  }

  /**
   * Overrides method.
   *
   * @inheritDoc
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if ($form_state->getErrors()) {
      return;
    }
    $values = $form_state->getValue($form['#parents']);

    foreach (array_keys($values) as $key) {
      if (!isset($values[$key])) {
        continue;
      }
      $this->configuration[$key] = $values[$key];
    }
  }

  /**
   * Overrides method.
   *
   * @inheritDoc
   *
   * @throws \Exception
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $url = parse_url($request->headers->get('referer'));
    $configuration = $this->getConfiguration();
    $message = $this->t('%url from CECA', ['%url' => $url]);
    \Drupal::logger('tpv_ceca')->notice($message);
    if ($url['host'] == $configuration['endpoint_domain']) {
      parent::onReturn($order, $request);
      $message = $this->t('Order %id has been approved on CECA', ['%id' => $order->id()]);
      \Drupal::logger('tpv_ceca')->notice($message);

      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
      $payment = $payment_storage->create([
        'state' => 'authorization',
        'amount' => $order->getTotalPrice(),
        'payment_gateway' => $this->parentEntity->id(),
        'order_id' => $order->id(),
      ]);
      \Drupal::logger('tpv_ceca')
        ->info('Saving Payment information. Transaction reference: ' . $order->id());
      $payment->save();
      \Drupal::logger('tpv_ceca')
        ->info('Payment information saved successfully. Transaction reference: ' . $order->id());
    }
    else {
      $message = $this->t('The referer is not a valid for order %order', ['%order' => $order->id()]);
      \Drupal::logger('tpv_ceca')->notice($message);
    }
  }

  /**
   * Overrides method.
   *
   * @inheritDoc
   */
  public function onCancel(OrderInterface $order, Request $request) {
    parent::onCancel($order, $request);
    $message = $this->t('Order %id has been canceled on CECA', ['%id' => $order->id()]);
    \Drupal::logger('my_module')->notice($message);
  }

}
